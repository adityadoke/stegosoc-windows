name 'stegosoc-windows'
maintainer 'Aditya Doke'
maintainer_email 'aditya.doke@gmail.com'
description 'Provides wazuh agent installation for windows'

version '0.1.0'

supports 'windows'

depends 'windows', '= 2.1.1'
depends 'ohai', '= 4.0.0' 
depends 'compat_resource', '>= 12.9.0'


